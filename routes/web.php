<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Welcome@index');
Route::get('/oferta/reservation/{id}', 'ReservationsController@index');
Route::post('/oferta/reservation/{id}/save', 'ReservationsController@saveReservation');
Route::get('/oferta/reservation_thanks', ['as' => 'reservationThanks', 'uses' =>'ReservationsController@getThanks']);
Route::get('/oferta/{id}', 'productCard@index');
Route::get('/oferta', 'OffersController@index');
Route::get('verifyemail/{reservation_id}/{token}', 'ReservationsController@verify' );


Auth::routes();

Route::namespace('Admin')->prefix('panel')->group(function () {
    Route::get('', 'HomeController@index');
    Route::get('offers', ['as' => 'offers', 'uses' => 'OffersController@index']);
    Route::post('offers/delete', 'OffersController@postAjaxDelete');
    Route::get('offers/add', 'OffersController@getAdd');
    Route::get('offers/edit,{id}', 'OffersController@getEdit');
    Route::post('offers/edit,{id}/save', 'OffersController@postEditSave');
    Route::post('offers/add/save', 'OffersController@postSave');
    Route::get('reservations', 'ReservationsController@index');
    Route::get('reservations/preview,{id}', 'ReservationsController@preview');
    Route::get('slider', ['as'=>'slider', 'uses' => 'SliderController@index']);
    Route::get('slider/add', 'SliderController@getAdd');
    Route::post('slider/add/save', 'SliderController@postSaveSlide');
    Route::post('slider/delete', 'SliderController@postAjaxDelete');
    Route::get('settings', 'SettingsController@index');
    Route::post('settings/save', 'SettingsController@postSave');
    Route::get('logout', 'HomeController@logout');
});

