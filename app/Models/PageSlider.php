<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PageSlider extends Model
{
    use SoftDeletes;

    protected $table = 'page_slider';
    protected $fillable = ['image', 'title', 'description', 'type'];

    public static function getSlides($type)
    {
        $query = self::where('type', $type)->get();

        return $query;
    }
}
