<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    protected $fillable = ['name','surname','address_city', 'address_street', 'address_home','zip_code','peoples','addictional_info', 'email', 'email_token'];
    //

    public function offer()
    {
        return $this->hasOne('App\Models\Offers','id','offer_id');
    }

    public static function getVerified(){
        $query = self::with('offer')->where('verified',1)->get();       
        return $query;
    }
}
