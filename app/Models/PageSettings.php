<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageSettings extends Model
{
    protected $table = 'page_settings';
    protected $fillable = ['page_id','footer_first','footer_second','footer_third','footer_fourth','homepage_offer_count', 'offerpage_offer_count'];
    
}
