<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offers extends Model
{
    use SoftDeletes;


    protected $table = 'offers';
    public $timestamps = false;
    protected $properties = [
        'id',
        'picture',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'title',
        'description',
        'price',
        'status',
        'place',
        'departure_date',
        'duration_days',
        'number_of_people',
        'additional_info',
        'attractions',
        'picture'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $nullable  = [
        'deleted_at',
        'deleted_by',
        'updated_by',
        'updated_at',
    ];
  
    protected $fillable = [
        'id',
        'picture',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'deleted_by',
        'deleted_at',
        'title',
        'description',
        'price',
        'status',
        'place',
        'departure_date',
        'duration_days',
        'number_of_people',
        'additional_info',
        'attractions'
    ];

    public static function getOffers($pagination, array $filter = [])
    {        
        $query = self::where('status', '2');
        $query = self::filter($query, $filter);
        // dd($query);
        return $query->paginate($pagination);
    }

    public static function getAdminOffers()
    {        
        $query = self::where('status', '2')->paginate(15);
        // dd($query);
        return $query;
    }


    public static function getOfferById($id)
    {
        $query = self::where('id', $id);
        
        return $query->first();
    }

    public static function getAllCity()
    {
        $query = self::select('place')->distinct()->get();
        
        return $query;
    }

    private static function filter($query, array $filter = [])
    {
        if (array_has($filter, 'place'))
        {
            $query->where('place', '=', array_get($filter, 'place'));
        }
        if (array_has($filter, 'from'))
        {
            $query->where('departure_date', '>=', array_get($filter, 'from'));
        }
        if (array_has($filter, 'to'))
        {
            $query->where('departure_date', '<', array_get($filter, 'to'));
        }
        if (array_has($filter, 'peoples'))
        {
            $query->where('number_of_people', '>=', array_get($filter, 'peoples'));
        }


        
        return $query;
    }

}
