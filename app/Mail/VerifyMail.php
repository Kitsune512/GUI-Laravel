<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Reservations;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;
    public $reservation_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation_id)
    {
        $this->reservation_id = $reservation_id;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token = Reservations::find($this->reservation_id);
        return $this->view('emails.reservationConfirm')->with([
        'email_token' => $token->email_token,
        'reservation_id' => $token->id,
            ]);
    }
}
