<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\PageSettings;

class OffersController extends Controller
{
	public function index()
	{
		$settings = PageSettings::where('page_id', config('app.page_id'))->first();
		if($settings['logo'])
        {
            $logo = Storage::disk('public')->url($settings['logo']);
        }
        else
        {
            $logo = '';
        }
		if(!empty($settings['homepage_offer_count']))
		{
		  $pagination = $settings['offerpage_offer_count'];
		}
		else
		{
		  $pagination = 10;
		}

		$offers = Offers::getOffers($pagination);
		if(!empty($offers))
		{
			foreach ($offers as $key => $offer) {
				$offer['image_url'] =  Storage::disk('images')->url($offer["picture"]);
			}
		}

		return view('layouts.product.offer', compact('logo_url', 'offers', 'logo'));
	}

}
