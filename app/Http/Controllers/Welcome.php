<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offers;
use App\Models\PageSlider;
use App\Models\PageSettings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\User;

use Illuminate\Support\Facades\Mail;

class Welcome extends Controller
{


  public function index()
  {
      
    $settings = PageSettings::where('page_id', config('app.page_id'))->first();
    //logo
    if($settings['logo'])
    {
        $logo = Storage::disk('public')->url($settings['logo']);
    }
    else
    {
        $logo = '';
    }
    //ilosc wyswietlanych ofert
    if(!empty($settings['homepage_offer_count']))
    {
      $pagination = $settings['homepage_offer_count'];
    }
    else
    {
      $pagination = 10;
    }

    $filters = array();
    if(!empty($_GET['from']))
    {
        $filters['from'] = $_GET['from'];
    }
    if(!empty($_GET['to']))
    {
        $filters['to'] = $_GET['to'];
    }
    if(!empty($_GET['place']))
    {
        $filters['place'] = $_GET['place'];
    }
    if(!empty($_GET['peoples']))
    {
        $filters['peoples'] = $_GET['peoples'];
    }
    
    $cities = Offers::getAllCity();

    $offers = Offers::getOffers($pagination, $filters);
    if(!empty($offers))
    {
        foreach($offers as $offer)
        {
          $max_length = 200;

            if (strlen($offer['description']) > $max_length)
            {
                $offset = ($max_length - 3) - strlen($offer['description']);
                $offer['description'] = substr($offer['description'], 0, strrpos($offer['description'], ' ', $offset)) . '...';
            }
            $offer['image_url'] =  Storage::disk('images')->url($offer["picture"]);
            
        }
    }
    //type 1 = main slider
    //type 2 = bottom slider  
    
    $slider = PageSlider::getSlides(1);
    if(!empty($slider))
    {
        $slider->each(function ($item) {
            $item['slide_url'] = Storage::disk('slider')->url($item['image']);
        });
    }


    $slider_bottom = PageSlider::getSlides(2);
    if(!empty($slider_bottom))
    {
        $slider_bottom->each(function ($item) {
            $item['slide_url'] = Storage::disk('slider')->url($item['image']);
        });
    }


    return view('layouts.home.welcome', compact('offers', 'slider', 'slider_bottom', 'logo', 'cities'));
  
  }

}