<?php

namespace App\Http\Controllers\Admin;
use App\Models\Offers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class OffersController extends Controller
{
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offers::getAdminOffers();
        if(!empty($offers))
        {
            $max_length = 200;
            foreach($offers as $offer)
            {
            
                if (strlen($offer['description']) > $max_length)
                {
                    $offset = ($max_length - 3) - strlen($offer['description']);
                    $offer['description'] = substr($offer['description'], 0, strrpos($offer['description'], ' ', $offset)) . '...';
                }
                $offer['image_url'] =  Storage::disk('images')->url($offer["picture"]);

            }
        }
        return view('layouts.admin.offers', compact('offers'));
    }


    public static function postAjaxDelete()
    {
        $id = $_POST['id'];
        if(\Auth::check())
        {
            $entity = Offers::find($id);
            $entity->deleted_by = \Auth::id();
            $entity->delete();
        }
    }

    public function getAdd()
    {
        return view('layouts.admin.addOffer');
    }

    public function postSave(Request $request)
    {
        $entity = new Offers;
        $entity->fill($request->all());
        $photo = Storage::putFile('public/images', $request->picture);
        $photo = basename($photo);
        $entity->picture = $photo;
        $entity->status = 2;
        $entity->save();

        return redirect()->route('offers');
    }

    public function getEdit($id)
    {
        $offer = Offers::find($id);
        return view('layouts.admin.editOffer', compact('offer'));
    }

    public function postEditSave(Request $request, $id)
    {
        $entity = Offers::find($id);
        $entity->fill($request->all());
        if(!empty($request->picture))
        {
            $photo = Storage::putFile('public/images', $request->picture);
            $photo = basename($photo);
            $entity->picture = $photo;
        }  
        $entity->update();
        return redirect()->route('offers');
    }
}
