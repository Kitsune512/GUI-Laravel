<?php

namespace App\Http\Controllers\Admin;
use App\Models\Reservations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;



class ReservationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations='';
        $reservations = Reservations::getVerified();
    
        return view('layouts.admin.reservations', compact('reservations'));
    }

    public function preview($id)
    {
        $reservation = Reservations::find($id);
        return view('layouts.admin.reservationsPreview', compact('reservation'));
    }
}
