<?php

namespace App\Http\Controllers\Admin;
use App\Models\PageSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class SliderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider_main = PageSlider::where('type', 1)->get();
        if(!empty($slider_main))
        {
            $slider_main->each(function ($item) {
                $item->image_url = Storage::disk('slider')->url($item->image);
            });
        }

        $slider_second = PageSlider::where('type', 2)->get();
        {
            $slider_second->each(function ($item) {
                $item->image_url = Storage::disk('slider')->url($item->image);
            });
        }

        return view('layouts.admin.slider', compact('slider_main','slider_second'));
    }

    public static function getAdd()
    {
        return view('layouts.admin.addSlide');
    }

    public function postSaveSlide(Request $request)
    {
        // stworzenie nowego obiektu modelu klasy PageSlider        
        $entity = new PageSlider;

        // dopasowanie przesłanych danych metodą POST do pól w modelu
        $entity->fill($request->all());

        // przesłanie pliku do katalogu przechowywania
        $photo = Storage::putFile('public/slider', $request->image);

        // pobranie zakodowanej nazwy pliku wygenerowanej automatycznie metodą putFile
        $photo = basename($photo);

        // przypisanie do pola "image" wygenerowanej nazwy
        $entity->image = $photo;

        // przypisanie autora do pola "created_by"
        $entity->created_by = \Auth::id();

        // zapisanie wpisu do bazy
        $entity->save();

        // przekierowanie użytkownika na stronę zarządzania rotatorami
        return redirect()->route('slider');
    }

    public function postAjaxDelete()
    {
        $id = $_POST['id'];
        $entity = PageSlider::find($id);
        $entity->deleted_by = \Auth::id();
        $entity->delete();
    }
    
}
