<?php

namespace App\Http\Controllers\Admin;
use App\Models\Offers;
use App\Models\Reservations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers_count = Offers::where('deleted_at', NULL)->count();
        $reservations_count = Reservations::where('deleted_at', NULL)->where('verified', 1)->count();

        return view('layouts.admin.index', compact('offers_count','reservations_count'));
    }

    public function getOffers()
    {
        $pagination = 2;
        $offers = Offers::getOffers();
        if(!empty($offers))
        {
            foreach($offers as $offer)
            {
                $offer['image_url'] =  Storage::disk('images')->url($offer["picture"]);
                
            }
        }
        return view('layouts.admin.offers', compact('offers'));
    }


    public function logout(Request $request) {
        \Auth::logout();
        return redirect('/login');
      }
}
