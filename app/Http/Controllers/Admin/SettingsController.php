<?php

namespace App\Http\Controllers\Admin;
use App\Models\PageSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;



class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $old_data = array();
        $old_data = PageSettings::where('page_id', config('app.page_id'))->first();
        if(!empty($old_data))
        {
            $old_data->toArray();
        }
        return view('layouts.admin.settings', compact('old_data'));
    }

    public function postSave(Request $request)
    {
        $model = PageSettings::firstOrCreate(['page_id' => config('app.page_id')]);
        $model->fill($request->all());
        $model->update();
        
        return \Redirect::back();
    }
    
}
