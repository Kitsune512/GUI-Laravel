<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PageSettings;
use App\Models\Offers;
use App\Models\Reservations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SendVerificationEmail;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;

class ReservationsController extends Controller 
{

	
  public function index($id)
  {
        $settings = PageSettings::where('page_id', config('app.page_id'))->first();
        //logo
        if($settings['logo'])
        {
            $logo = Storage::disk('public')->url($settings['logo']);
        }
        else
        {
            $logo = '';
        }

        $offer = Offers::getOfferById($id);
        return view('layouts.product.reservation', compact('logo','offer'));
	}

	public function getThanks(){
		$settings = PageSettings::where('page_id', config('app.page_id'))->first();
		if($settings['logo'])
		{
			$logo = Storage::disk('public')->url($settings['logo']);
		}
		else
		{
			$logo = '';
		}
			return view('layouts.product.reservationThanks', compact('logo'));
	}
	
	public function saveReservation(Request $request, $id)
	{
		$entity = new Reservations;
		$entity->fill($request->all());
        $entity->offer_id = $id;
        $entity->email_token = base64_encode($request->email);
        $entity->save();
        Mail::to($request->email)->send(new VerifyMail($entity->id));
		return redirect()->route('reservationThanks');
    }
    
    public function verify($reservation_id, $token)
    {
        $entity = Reservations::where('email_token', $token)->where('id', $reservation_id)->first();
        $entity->verified = 1;
        $entity->update();


        $settings = PageSettings::where('page_id', config('app.page_id'))->first();
		if($settings['logo'])
		{
			$logo = Storage::disk('public')->url($settings['logo']);
		}
		else
		{
			$logo = '';
        }
        
        return view('layouts.product.verifyReservation', compact('logo'));
      
    }
}
