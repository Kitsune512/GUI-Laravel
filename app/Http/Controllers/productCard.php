<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offers;
use App\Models\PageSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class productCard extends Controller
{
    public function index($id)
    {
        $settings = PageSettings::where('page_id', config('app.page_id'))->first();
        //logo
        if($settings['logo'])
        {
            $logo = Storage::disk('public')->url($settings['logo']);
        }
        else
        {
            $logo = '';
        }
        $offer = Offers::getOfferById($id);
        $offer['image_url'] =  Storage::disk('images')->url($offer["picture"]);
        $logo_url = Storage::url('logo.png');
        return view('layouts.product.product', compact('offer', 'logo'));
    }

}
