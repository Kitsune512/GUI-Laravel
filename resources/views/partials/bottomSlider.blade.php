<div class="grid-x slider-bottom">
    <div class="small-12 large-12 medium-12 cell">
        <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
            <div class="orbit-wrapper">
                <div class="orbit-controls">
                    <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
                    <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
                </div>
                @if(!empty($slider_bottom))
                    <ul class="orbit-container">
                        @foreach($slider_bottom as $slide)
                        <li class="is-active orbit-slide">
                            <figure class="orbit-figure">
                                <img class="orbit-image" src="{{ $slide['slide_url'] }}" alt="Space">
                            </figure>
                        </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
