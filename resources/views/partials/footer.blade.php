<div class="footer">
    <div class="grid-x">
        <div class="large-2 cell medium-uncollapse large-collapse"></div>
        <div class="small-12 medium-2 large-2 cell">
            @if(!empty($settings->footer_first))
                {!! $settings->footer_first !!}
            @endif
        </div>
        <div class="small-12 medium-2 large-2 cell">
            @if(!empty($settings->footer_second))
                {!! $settings->footer_second !!}
            @endif
        </div>
        <div class="small-12 medium-2 large-2 cell">
            @if(!empty($settings->footer_third))
                {!! $settings->footer_third !!}
            @endif
        </div>
        <div class="small-12 medium-2 large-2 cell">
            @if(!empty($settings->footer_fourth))
                {!! $settings->footer_fourth !!}
            @endif</div>
    </div>

    <div class="copyright"> {!! __('CMS stworzony przez Damiana Ziarnika') !!} </div>
</div>