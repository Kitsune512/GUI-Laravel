
<div class="cell medium-2 medium-cell-block-y menu ">
<h2>Menu</h2>
    <ul>
        <li><a href="/panel" active>{!! __('Strona główna') !!} </a></li>
        <li><a href="/panel/offers" >{!! __('Oferty') !!} </a></li>
        <li><a href="/panel/reservations"> {!! __('Rezerwacje') !!} </a></li>
        <li><a href="/panel/slider"> {!! __('Rotator') !!} </a></li>
        <li><a href="/panel/settings"> {!! __('Ustawienia') !!} </a></li>
        <li><a href="/panel/logout">{!! __('Wyloguj') !!}</a></li>

    </ul>
</div>
    