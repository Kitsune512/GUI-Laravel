<?php
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Welcome;

?>


<!doctype html>



<html class="no-js" lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Biuro podróży</title>
  <link rel="stylesheet" href="{{asset('public/css/foundation.css')}} /">
  <link rel="stylesheet" href="{{asset('public/css/app.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/animate.css')}}">

</head>
<body class="front">
    
@php
	use App\Models\PageSettings;
    $settings = PageSettings::where('page_id', config('app.page_id'))->first();
@endphp
@include('partials.header')
@include('partials.mainSlider')
@include('layouts.home.breadcrumbs')
@include('layouts.home.filters')


  <div class="cell small-12 medium-10 large-6 medium-cell-block-y">
        @if($offers->isNotEmpty())
        <h4> {!! __('Ostatnie oferty') !!} </h4>
        @foreach($offers as $offer)
                    <div class="grid-x offer wow bounceIn">
                        <div class="cell">
                            <div class="grid-x">
                                <div class="medium-4 cell"><img src="{{ $offer['image_url'] }}" class="thumbs"></div>
                                    <div class="medium-6 cell">
                                        <div class="grid-x info">
                                            <div class="cell title">{{ $offer['title']}} </div>
                                            <div class="cell description">{{ $offer['description']}}</div>
                                        </div>
                                    </div>
                                    <div class="medium-2 cell">
                                    <div class="grid-x">
                                    <div class="cell price">Cena: {{$offer['price']}} zł</div>
                                        <div class="cell"><a href="oferta/reservation/{{$offer['id']}}" class="button medium expanded rezerwacja">Zarezerwuj</a></div>
                                        <div class="cell"><a href="oferta/{{$offer['id']}}" class="hollow button medium expanded">Szczegóły</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            @endforeach
        @endif
  </div></div>
@include('partials.bottomSlider')
@include('partials.footer')
</div>

        <script src="{{asset('public/js/vendor/jquery.js')}}"></script>
        <script src="{{asset('public/js/vendor/what-input.js')}}"></script>
        <script src="{{asset('public/js/vendor/foundation.js')}}"></script>
        <script src="{{asset('public/js/app.js')}}"></script>
        <script> $(document).foundation();</script>
        <script src="js/wow.min.js"></script>
        <script>
        new WOW().init();
        </script>
    </body>
</html>
