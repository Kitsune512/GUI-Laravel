


    <div class="cell small-2 small-cell-block-y hide-for-small-only">
      <div class="grid-x">
       <h2>Filtry</h2>
        <div class="cell">
        <form method="GET" action="{{url()->current()}}">
          <div class="grid-container">
            <div class="cell">
             
                <label>Od
                  @if(isset($_GET['from']))
                    <input type="date" name="from" placeholder="Od" value="{{$_GET['from']}}">
                  @else
                    <input type="date" name="from" placeholder="Od">
                  @endif
                </label>
              
            </div>
            <div class="cell">
              
                <label>Do
                  @if(!empty($_GET['to']))
                    <input type="date" name="to" placeholder="Do" value="{{$_GET['to']}}">
                  @else
                    <input type="date" name="to" placeholder="Do" >
                  @endif
                </label>
              
            </div>
          </div>
       
      </div>
        <div class="cell"><label>Skąd
          <select name="place">
            <option value=''>wybierz</option>
            @foreach($cities as $city)
          <option value="{{ $city->place }}" <?php if(!empty($_GET['place']) && $_GET['place'] == $city->place) echo('selected') ?>> {{ $city->place }}</option>
            @endforeach
          </select>
        </label>
      </div>
      
      <div class="cell">
        <label>
          Ile osób
          @if(!empty($_GET['peoples']))
            <input type="number" name="peoples" value="{{$_GET['peoples']}}">
          @else
            <input type="number" name="peoples">
          @endif
        </label>
      </div>
      <div class="cell"><button type="submit" class="button expanded szukajka">Szukaj</a></div>
    </form>





    </div>
  </div>
