
@extends('layouts.layout')

@section('header')
<div class="grid-x">
    <div class="large-2 small-12  medium-2 small-offset-2 medium-offset-0 large-offset-0 cell"></div>
        <div class="small-12 medium-6 large-4 cell">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="/">Strona Główna</a></li>
                    <li><a href="/oferta">Oferta</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> {{ $offer['title']}}
                    </li>
                </ul>
            </nav>
        </div>
    </div>

@endsection


@section('content')
    <div class="product-card">
        <div class="grid-x">
            <div class="large-2 medium-2"></div>
            <div class="large-4 gallery"><img src="{{$offer['image_url']}}"></div>
            <div class="large-4 small-12 description">
                <div class="grid-x">
                    <div class="cell small-offset-2 medium-offset-0 large-offset-0"><p> {{$offer['title']}}  </p></div>
                </div>
                <div class="grid-x">
                        <table class="hover">
                            <tbody>
                                @if(!empty($offer['price']))
                                    <tr>
                                        <td> {!!__('Cena') !!}</td>
                                        <td> {{ $offer['price'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['place']))
                                    <tr>
                                        <td> {!!__('Miejsce wycieczki') !!}</td>
                                        <td> {{ $offer['place'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['departure_date']))
                                    <tr>
                                        <td> {!!__('Data wylotu') !!}</td>
                                        <td> {{ $offer['departure_date'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['duration_days']))
                                    <tr>
                                        <td> {!!__('Czas trwania wycieczki (dni)') !!}</td>
                                        <td> {{ $offer['duration_days'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['number_of_people']))
                                    <tr>
                                        <td> {!!__('Liczba osób') !!}</td>
                                        <td> {{ $offer['number_of_people'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['additional_info']))
                                    <tr>
                                        <td> {!!__('Dodatkowe informacje') !!}</td>
                                        <td> {{ $offer['additional_info'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['attractions']))
                                    <tr>
                                        <td> {!!__('Atrakcje') !!}</td>
                                        <td> {{ $offer['attractions'] }}</td>
                                    </tr>
                                @endif
                                
                            </tbody>
                        </table>
                </div> 
                <div class="grid-x">
                <div class="cell small-offset-2"><a href="{{url('/')}}/oferta/reservation/{{$offer['id']}}" class="button">Zarezerwuj</a></div>
                </div> 
                
            </div>
        </div>
        <div class="grid-x">
            <div class="grid-x large-offset-2 offer-description-div">
                    <h2 class="cell"> Opis </h2>
                    <div class="cell large-6 small-8 small-offset-2 medium-offset-2 large-offset-0  offer-description">
                            <div class="cell end">{{ $offer['description'] }}</div>
                    </div>
                </div>
        </div>
      
    </div>
@endsection 

