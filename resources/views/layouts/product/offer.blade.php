@extends('layouts.layout')


@section('header')
<div class="grid-x">
    <div class="large-2 cell"></div>
    <div class="small-4 cell">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
                <li><a href="#"></a>
                    <span class="show-for-sr">Current: </span> Strona główna/
                </li>
            </ul>
        </nav>
    </div>
</div>
@endsection



@section('content')
<div class="grid-x grid-padding-x offer-page ">
    <div class="grid-x wow bounceIn large-offset-2 large-8">
        @foreach ($offers as $offer)
        <div class="medium-3 large-3 column column-block offer card">
            <div class="foto">
                <img src="{{ $offer['image_url']}}">
            </div>
            <h4>{{$offer['title']}}</h4>
            <p class="price">{{$offer['price']}} zł</p>
            <p>{{$offer['descro[topm']}}</p>

        <a class="button" href="{{ url('/')}}/oferta/{{$offer['id']}}"> Szczegóły</a>
        </div>

        @endforeach
    </div>
</div>


<div class="grid-x">
    <div class="auto cell text-center">
        {{ $offers->links() }}
    </div>
</div>
@endsection