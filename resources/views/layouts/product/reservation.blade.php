
@extends('layouts.layout')

@section('header')
<div class="grid-x">
    <div class="large-2 small-12  medium-2 small-offset-2 medium-offset-0 large-offset-0 cell"></div>
        <div class="small-4 small-12 medium-6 large-4 cell cell">
            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="/">Strona Główna</a></li>
                    <li><a href="/oferta">Oferta</a></li>
                    <li>
                        <span class="show-for-sr">Current: </span> {{ $offer['title']}}
                    </li>
                </ul>
            </nav>
        </div>
    </div>

@endsection


@section('content')

    <div class="product-card">
        
        <div class="grid-x">
            <div class="large-2 medium-2"></div>
            <div class="large-3 small-8 small-offset-2 large-offset-0  medium-offset-0 medium-3 gallery">
                <h2> {!! __('Rezerwacja') !!} </h2>
                <form action="{{url()->current()}}/save" method="POST" class="reservation-form">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <label for="name">{!! __('Imię') !!}</label>
                    <input type="text" name="name" required>
                    
                    <label for="surname">{!! __('Nazwisko') !!}</label>
                    <input type="text" name="surname" required>
                    
                    <label for="email">{!! __('Email') !!}</label>
                    <input type="email" name="email" required>

                    <label for="address_city">{!! __('Miasto') !!}</label>
                    <input type="text" name="address_city" required>
                    
                    <label for="address_street">{!! __('Ulica') !!}</label>
                    <input type="text" name="address_street" required>
                    
                    <label for="address_home">{!! __('numer domu') !!}</label>
                    <input type="text" name="address_home">
                    
                    <label for="zip-code">{!! __('kod pocztowy') !!}</label>
                    <input type="text" name="zip-code" required>
                    
                    <label for="peoples">{!! __('ilosć osób') !!}</label>
                    <input type="text" name="peoples" required>
                    
                    <label for="additional_info">{!! __('Dodatkowe informacje') !!}</label>
                    <input type="text" name="additional_info">
            

                    <button type="submit" class="button">{!! __('Zarezerwuj') !!}</button>
                </form>
            </div>
            <div class="large-1"></div>
            <div class="medium-4 description small-8 small-offset-2 medium-offset-0 large-offset-0 large-4">
                <div class="grid-x">
                    <div class="cell"><p> {{$offer['title']}}  </p></div>
                </div>
                <div class="grid-x">
                        <table class="hover">
                            <tbody>
                                @if(!empty($offer['price']))
                                    <tr>
                                        <td> {!!__('Cena') !!}</td>
                                        <td> {{ $offer['price'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['place']))
                                    <tr>
                                        <td> {!!__('Miejsce wycieczki') !!}</td>
                                        <td> {{ $offer['place'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['departure_date']))
                                    <tr>
                                        <td> {!!__('Data wylotu') !!}</td>
                                        <td> {{ $offer['departure_date'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['duration_days']))
                                    <tr>
                                        <td> {!!__('Czas trwania wycieczki (dni)') !!}</td>
                                        <td> {{ $offer['duration_days'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['number_of_people']))
                                    <tr>
                                        <td> {!!__('Liczba osób') !!}</td>
                                        <td> {{ $offer['number_of_people'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['additional_info']))
                                    <tr>
                                        <td> {!!__('Dodatkowe informacje') !!}</td>
                                        <td> {{ $offer['additional_info'] }}</td>
                                    </tr>
                                @endif
                                @if(!empty($offer['attractions']))
                                    <tr>
                                        <td> {!!__('Atrakcje') !!}</td>
                                        <td> {{ $offer['attractions'] }}</td>
                                    </tr>
                                @endif
                                
                            </tbody>
                        </table>
                </div> 
                
            </div>
        </div>      
    </div>
@endsection 

