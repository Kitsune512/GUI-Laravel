<!doctype html>
<html class="no-js" lang="en" dir="ltr">


	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Biuro podróży</title>
		<link rel="stylesheet" href="/css/foundation.css">
		<link rel="stylesheet" href="/css/app.css">
		{{-- <link rel="stylesheet" href="/css/style.css"> --}}
		<link rel="stylesheet" href="/css/animate.css">
	
	</head>

	<body class="front">
		@php
			use App\Models\PageSettings;
			$settings = PageSettings::where('page_id', config('app.page_id'))->first();
		@endphp
	
		

			@include('partials.header')

			@yield('header')
			@yield('content')
			@include('partials.footer')

			<script src="js/vendor/foundation.js"></script>
			<script src="js/vendor/what-input.js"></script>
			<script src="js/app.js"></script>
			<script src="{{asset('js/vendor/jquery.js')}}"></script>
			<script>
				$(document).foundation();
			</script>
			<script src="js/wow.min.js"></script>
			<script>
				new WOW().init();
			</script>
	</body>

</html>