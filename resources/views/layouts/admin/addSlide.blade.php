@extends('layouts.admin.layout')

@section('body')
<h2> Dodawanie slidera </h2>
<form action="{{url()->current()}}/save" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
     
        <label for="title">{!! __('Tytuł') !!} </label>
        <input type="text" name="title"  required="required">
  
        <label for="description">{!! __('Opis') !!}</label>
        <input type="text" name="description"  required="required">

        <label for="image">{!! __('Zdjęcie') !!}</label>
        <input type="file" name="image" accept='image/*' required="required">
   
         <label for="type">{!! __('Typ') !!}</label>
        <select name="type">
            <option value="1">Slider główny</option>
            <option value="2">Slider dolny</option>
        </select>
    
    <button class="button" type="submit"> Zapisz </button>

</form>
@endsection
