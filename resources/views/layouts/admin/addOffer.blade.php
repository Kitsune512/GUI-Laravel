@extends('layouts.admin.layout')

@section('body')
<h2> Dodawanie oferty </h2>
<form action="{{url()->current()}}/save" method="POST" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
     
        <label for="title">{!! __('Tytuł') !!} </label>
        <input type="text" name="title"  required="required">
  
        <label for="description">{!! __('Opis') !!}</label>
        <input type="text" name="description"  required="required">
  
        <label for="price">{!! __('Cena (zł)') !!}</label>
        <input type="number" name="price"  required="required">

        <label for="photo">{!! __('Zdjęcie') !!}</label>
        <input type="file" name="picture" accept='image/*'>

        <label for="place">{!! __('Miejsce') !!}</label>
        <input type="text" name="place"  required="required" >
  
        <label for="departure_date">{!! __('Data wylotu') !!}</label>
        <input type="date" name="departure_date"  required="required">
    
        <label for="duration_days">{!! __('Czas trwania w dniach') !!}</label>
        <input type="number" name="duration_days"  required="required">
  
        <label for="number_of_people">{!! __('Liczba ludzi') !!}</label>
        <input type="number" name="number_of_people"  required="required">
  
        <label for="additional_info">{!! __('Dodatkowe informacje') !!}</label>
        <input type="text" name="additional_info">
   
         <label for="attractions">{!! __('Atrakcje') !!}</label>
        <input type="text" name="attractions">
    
    
    <button class="button" type="submit"> Zapisz </button>

</form>
@endsection
