@extends('layouts.admin.layout')

@section('body')
<h1> Oferty </h1>
<a type="button" href="{{url()->current()}}/add" class="button">Dodaj ofertę</a>
    <div class="tabs-content" data-tabs-content="example-tabs">
      <div class="tabs-panel is-active" id="panel1">
        <table>
          <thead>
            <tr>
              <th width="150">Zdjecie</th>
              <th width="150">Tytul</th>
              <th>Opis</th>
              <th width="150">Cena</th>
             
              <th width="150">Usuń</th>
            </tr>
          </thead>
          <tbody>
         
            @foreach($offers as $offer)
              <tr>
              <td><img src="{{ $offer['image_url'] }}"></td>
              <td><a href="offers/edit,{{$offer['id']}}">{{ $offer['title']}}</a></td>
              <td>{{ $offer['description']}}</td>
              <td>{{ $offer['price'] }}</td>
              <td><a type="button" class="delete alert button" id="{{$offer['id']}}">Usuń</a></td>
              </tr>
              
            @endforeach
			

          </tbody>
		  </table>
		</div>
		{{ $offers->links() }}
    </div>
      </div>
  
      @endsection

      @push('scripts')
        <script type="text/javascript">

		$( document ).ready(function() {
          $('.delete').on('click', function(){
            var id = $(this).attr('id');
            $.ajax({
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              url : 'offers/delete',
              data : { id : id },
              type : 'POST',
              success : function() {
                location.reload();
                    },            
                  });

		})
		});
        


        </script>
      @endpush