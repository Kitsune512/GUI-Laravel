@extends('layouts.admin.layout')
@section('body')
<div>
    <h1> Ustawienia </h1>
</div>
<div>
    <form action="{{url()->current()}}/save" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="footer_first">{!! __('Stopka - sekcja 1') !!}</label>
        <textarea name="footer_first" rows="3" cols="40">{{ $old_data['footer_first'] }}</textarea>
        <label for="footer_second">{!! __('Stopka - sekcja 2') !!}</label>
        <textarea name="footer_second" rows="3" cols="40">{{ $old_data['footer_second'] }}</textarea>
        <label for="footer_third">{!! __('Stopka - sekcja 3') !!}</label>
        <textarea name="footer_third" rows="3" cols="40">{{ $old_data['footer_third'] }}</textarea>
        <label for="footer_fourth">{!! __('Stopka - sekcja 4') !!}</label>
        <textarea name="footer_fourth" rows="3" cols="40">{{ $old_data['footer_fourth'] }}</textarea>
        <label for="homepage_offer_count">{!! __('Liczba ofert wyswietlanych na ofert') !!}</label>
        <input type="number" name="homepage_offer_count" value="{{$old_data['homepage_offer_count']}}">
        <label for="offerpage_offer_count">{!! __('Liczba ofert wyswietlanych na ofert (wielkosc paginacji)') !!}</label>
        <input type="number" name="offerpage_offer_count" value="{{$old_data['offerpage_offer_count']}}">
        <button type="submit" class="button">Zapisz</button>
    </form>
</div>


        
@endsection