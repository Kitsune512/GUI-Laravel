@extends('layouts.admin.layout')
@section('body')
<h1> Rotatory </h1>
<ul class="tabs" data-tabs id="slide-tabs">
        <li class="tabs-title is-active"><a data-tabs-target="panel1" href="#panel1" aria-selected="true">Slider górny</a></li>
        <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Slider dolny</a></li>
</ul>
<div class="tabs-content" data-tabs-content="slide-tabs">
    <div class="tabs-panel is-active" id="panel1">
        <a href="{{url()->current()}}/add" class="button">Dodaj zdjęcie </a>
        <table>
            <thead>
                    <tr>
                    <th width="150">Zdjecie</th>
                    <th width="150">Tytul</th>
                    <th>Opis</th>
                    <th width="150">Usuń</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($slider_main))
                    @foreach($slider_main as $slider)
                    <tr>
                        <td><img src="{{$slider->image_url}}"></td>
                        <td>{{$slider->title}}</td>
                        <td>{{$slider->description}}</td>
                        <td><a type="button" class="delete alert button" id="{{$slider['id']}}" >Usuń</a></td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="tabs-panel" id="panel2">
            <a href="" class="button">Dodaj zdjęcie </a>
            <table>
                <thead>
                        <tr>
                        <th width="150">Zdjecie</th>
                        <th width="150">Tytul</th>
                        <th>Opis</th>
                        <th width="150">Usuń</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($slider_second))
                        @foreach($slider_second as $slider)
                        <tr>
                            <td><img src="{{$slider->image_url}}"></td>
                            <td>{{$slider->title}}</td>
                            <td>{{$slider->description}}</td>
                            <td><a type="button" class="delete alert button" id="{{$slider['id']}}">Usuń</a></td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">

$( document ).ready(function() {
    $('.delete').on('click', function(){
        var id = $(this).attr('id');
        $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url : 'slider/delete',
        data : { id : id },
        type : 'POST',
        success : function() {
            location.reload();
                },            
            });

    })
});



</script>
@endpush