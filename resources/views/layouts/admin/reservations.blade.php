@extends('layouts.admin.layout')
@section('body')
<h1> Rezerwacje </h1>
<table>
    <thead>
      <tr>
        <th width="200">Imie i nazwisko</th>
        <th>Oferta</th>
        <th width="150">Data</th>
        <th width="150">Kwota</th>

      </tr>
    </thead>
    <tbody>
   
      @foreach($reservations as $reservation)

        <tr>
          <td><a href="reservations/preview,{{$reservation['id']}}">{{$reservation['name'].' '.$reservation['surname'] }}</a></td>
          <td>{{$reservation['offer']['title']}}</td>
          <td>{{$reservation['created_at']}}</td>
          <td>{{$reservation['offer']['price']}} {{ __('zł')}}</td>

        </tr>
        
      @endforeach
      

    </tbody>
    </table>
@endsection