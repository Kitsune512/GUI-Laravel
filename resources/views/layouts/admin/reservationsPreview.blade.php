@extends('layouts.admin.layout')
@section('body')
<h1> Szczegóły rezerwacji </h1>
<table>
    <tbody>
        <tr>
            <td>{!! __('id') !!}</td>
            <td>{{ $reservation['id'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Imie') !!}</td>
            <td>{{ $reservation['name'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Nazwisko') !!}</td>
            <td>{{ $reservation['surname'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Miasto') !!}</td>
            <td>{{ $reservation['address_city'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Kod pocztowy') !!}</td>
            <td>{{ $reservation['zip_code'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Ulica') !!}</td>
            <td>{{ $reservation['address_street'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Numer domu') !!}</td>
            <td>{{ $reservation['address_home'] }}</td>
        </tr>
        <tr>
            <td>{!! __('email') !!}</td>
            <td>{{ $reservation['email'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Ilosc osób') !!}</td>
            <td>{{ $reservation['peoples'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Rezerwacja stworzona') !!}</td>
            <td>{{ $reservation['created_at']->format('Y-m-d') }}</td>
        </tr>
        <tr>
            <td>{!! __('Nazwa oferty') !!}</td>
            <td>{{ $reservation['offer']['title'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Miejsce docelowe') !!}</td>
            <td>{{ $reservation['offer']['place'] }}</td>
        </tr>
        <tr>
            <td>{!! __('Kwota') !!}</td>
        <td>{{ $reservation['offer']['price'] }}{{ __('zł') }}</td>
        </tr>
    
       
       

    </tbody>
</table>
@endsection