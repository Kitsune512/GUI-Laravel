
<!doctype html>
<html class="no-js" lang="en" dir="ltr">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Biuro podróży</title>
    <link rel="stylesheet" href="{{asset('/css/foundation.css')}}">
		<link rel="stylesheet" href="{{asset('/css/app.css')}}">
		<link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/animate.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
	</head>

	<body>
      {{-- @include('partials.header') --}}
      {{-- @include('partials.mainSlider') --}}
		<div class="panel">
			@yield('header')
			<div class="grid-y medium-grid-frame">
				<div class="cell medium-auto medium-cell-block-container">
					<div class="grid-x grid-padding-x left">
						@include('partials.admin.menu')
					<div class="cell medium-10 medium-cell-block-y">
						@yield('body')
					</div>
			</div>
			<div class="cell shrink footer">
				@include('partials.admin.footer')
			</div>
        </div>
    
      


      {{-- @include('partials.bottomslider') --}}
			{{-- @include('partials.footer') --}}


			

			<script src="{{asset('js/jquery.js')}}"></script>
			<script src="{{asset('js/vendor/what-input.js')}}"></script>
			<script src="{{asset('js/vendor/foundation.js')}}"></script>
			<script src="{{asset('js/app.js')}}"></script>
			<script>
				$(document).foundation();
      </script>
      @stack('scripts')
			<script src="{{asset('js/wow.min.js')}}"></script>
			<script>
				new WOW().init();
			</script>
</body>

</html>



