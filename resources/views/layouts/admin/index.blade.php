@extends('layouts.admin.layout')
@section('body')
<h1> Strona główna </h1>
<h3>Witaj {{\Auth::user()->email}}</h3><br>
<p>
    Ilość aktywnych ofert: {{$offers_count}}
</p>
<p>
    Ilość rezerwacji: {{$reservations_count}}
</p>
@endsection