@extends('layouts.app')

@section('content')


<center>

      


                    <form class="" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="">Adres E-Mail </label>

                            <div class="large-3 large-centered columns">

                                <input id="email" type="email" class="mediun-3 large-3 small-3" style="max-width:300px!important;" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Hasło</label>

                            <div class="">
                                <input id="password" placehoder="haslo" style="max-width:300px!important;" type="password" class="" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="large-3 large-centered columns">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Zapamiętaj
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="button">
                                    Zaloguj
                                </button>

                                <a class="button" href="{{ route('password.request') }}">
                                    Przypomnij hasło
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
</div>
</center>

@endsection
