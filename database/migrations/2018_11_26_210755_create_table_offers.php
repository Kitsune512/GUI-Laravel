<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOffers extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
      Schema::create('offers', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->text('picture');
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->text('title');
            $table->text('description');
            $table->integer('price');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExist('offers');
    }
}
