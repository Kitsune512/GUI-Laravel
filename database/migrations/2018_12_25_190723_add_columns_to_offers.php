<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->text('place');
            $table->date('departure_date');
            $table->integer('duration_days');
            $table->integer('number_of_people');
            $table->text('additional_info')->nullable();
            $table->text('attractions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('place');
            $table->dropColumn('departure_date');
            $table->dropColumn('duration_days');
            $table->dropColumn('number_of_people');
            $table->dropColumn('additional_info');
            $table->dropColumn('attractions');
        });
    }
}
