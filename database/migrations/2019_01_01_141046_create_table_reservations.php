<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->integer('offer_id');
            $table->timestamp('email_verified_at');
            $table->string('name');
            $table->string('surname');
            $table->string('address_home')->nullable();
            $table->string('address_street');
            $table->string('address_city');
            $table->string('zip_code')->nullable();
            $table->integer('peoples');
            $table->string('additional_info');
            $table->integer('phone');
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservations');
    }
}
