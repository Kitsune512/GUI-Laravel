<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePageSlider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('page_slider', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->text('image');
            $table->text('title')->nullable();
            $table->integer('description')->nullable();
            $table->date('created_at');
            $table->integer('created_by');
            $table->date('updated_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
        Schema::dropIfExist('page_slider');
   }
}
