<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePageSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('page_settings', function (Blueprint $table) {
            $table->increments('id')->autoIncrement();
            $table->integer('page_id');
            $table->date('created_at');
            $table->date('updated_at')->nullable();
            $table->string('footer_first')->nullable();
            $table->string('footer_second')->nullable();
            $table->string('footer_third')->nullable();
            $table->string('footer_fourth')->nullable();
            $table->integer('homepage_offer_count')->default(10);
            $table->integer('offerpage_offer_count')->default(10);
  
        });
    }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
        Schema::drop('page_settings');
   }
}
